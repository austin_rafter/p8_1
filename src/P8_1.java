import java.util.Scanner;

/*
* Austin Rafter
* 013433901
* 9/27/2020
*
* Take user input as number of ticks for each step
* print whether user has entered the correct number of ticks at each step to
* open the lock
 */

public class P8_1 {
    public static void main(String[] args) {
        System.out.println("Enter the first amount of right ticks: "); //prompt for number
        Scanner scanObject = new Scanner(System.in);
        int nFirstCorrectEnteredNumber = scanObject.nextInt(); //user enters an int

        //Check that user has entered a positive integer
        //Have them re enter if negative
        if(nFirstCorrectEnteredNumber < 0 ){
            System.out.println("Please enter a positive integer");
            nFirstCorrectEnteredNumber = scanObject.nextInt(); //user enters an int
        }
        System.out.println("Enter the first amount of left ticks: "); //prompt for number
        scanObject = new Scanner(System.in);
        int nSecondCorrectEnteredNumber = scanObject.nextInt(); //user enters an int

        //Check that user has entered a positive integer
        //Have them re enter if negative
        if(nSecondCorrectEnteredNumber < 0 ){
            System.out.println("Please enter a positive integer");
            nSecondCorrectEnteredNumber = scanObject.nextInt(); //user enters an int
        }
        System.out.println("Enter the second amount of right ticks: "); //prompt for number
        scanObject = new Scanner(System.in);
        int nThirdCorrectEnteredNumber = scanObject.nextInt(); //user enters an int

        //Check that user has entered a positive integer
        //Have them re enter if negative
        if(nThirdCorrectEnteredNumber < 0 ){
            System.out.println("Please enter a positive integer");
            nThirdCorrectEnteredNumber = scanObject.nextInt(); //user enters an int
        }

        ComboLock comboLockSecrets = new ComboLock(10, 15, 30);

        comboLockSecrets.turnRight(nFirstCorrectEnteredNumber);
        comboLockSecrets.turnLeft(nSecondCorrectEnteredNumber);
        comboLockSecrets.turnRight(nThirdCorrectEnteredNumber);
        if(comboLockSecrets.open()){
            System.out.println("You've succeeded");
        }else{
            System.out.println("That's not right. Try again");
        }
        /*
        While the combo entered is incorrect keep running until it is correct
         */
    while(!comboLockSecrets.open()) {
            System.out.println("Enter the first amount of right ticks: "); //prompt for number
            scanObject = new Scanner(System.in);
            int nFirstIncorrectEnteredNumber = scanObject.nextInt(); //user enters an int

            //Check that user has entered a positive integer
            //Have them re enter if negative
            if (nFirstIncorrectEnteredNumber < 0) {
                System.out.println("Please enter a positive integer");
                nFirstIncorrectEnteredNumber = scanObject.nextInt(); //user enters an int
            }
            System.out.println("Enter the first amount of left ticks: "); //prompt for number
            scanObject = new Scanner(System.in);
            int nSecondIncorrectEnteredNumber = scanObject.nextInt(); //user enters an int

            //Check that user has entered a positive integer
            //Have them re enter if negative
            if (nSecondIncorrectEnteredNumber < 0) {
                System.out.println("Please enter a positive integer");
                nSecondIncorrectEnteredNumber = scanObject.nextInt(); //user enters an int
            }
            System.out.println("Enter the second amount of right ticks: "); //prompt for number
            scanObject = new Scanner(System.in);
            int nThirdIncorrectEnteredNumber = scanObject.nextInt(); //user enters an int

            //Check that user has entered a positive integer
            //Have them re enter if negative
            if (nThirdIncorrectEnteredNumber < 0) {
                System.out.println("Please enter a positive integer");
                nThirdIncorrectEnteredNumber = scanObject.nextInt(); //user enters an int
            }

            ComboLock comboLockSecretsWrong = new ComboLock(10, 15, 30);

            comboLockSecretsWrong.reset();
            comboLockSecretsWrong.turnRight(nFirstIncorrectEnteredNumber);
            comboLockSecretsWrong.turnLeft(nSecondIncorrectEnteredNumber);
            comboLockSecretsWrong.turnRight(nThirdIncorrectEnteredNumber);
            if (comboLockSecretsWrong.open()) {
                System.out.println("You've succeeded");
                break;
            } else {
                System.out.println("That's not right. Try again");
            }
    }

    }
}
