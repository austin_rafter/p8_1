/*
*take 3 secret numbers as constructor
* take numbers and tick left or right the number of ticks
*if numbers that the lock is set on arent correct reset return false
* if they are equal to the secret numbers then reset and return true
 */

public class ComboLock {

    int nSecretOne;
    int nSecretTwo;
    int nSecretThree;

    int nTickCount;
    static int nTurnOnLock = 0;
    static int nRightTurnCalls = 0;

    int nSecretTwoCheck;
    int[] nSecretOneAndThreeChecks = new int[2];


    public ComboLock (int nSecretNumberOne, int nSecretNumberTwo, int nSecretNumberThree){
        nSecretOne = nSecretNumberOne;
        nSecretTwo = nSecretNumberTwo;
        nSecretThree = nSecretNumberThree;
    }

    /*
    reset all static numbers and number checks on reset call
     */
    public void reset(){
        nTurnOnLock = 0;
        nSecretTwoCheck = 0;
        nSecretOneAndThreeChecks[0] = 0;
        nSecretOneAndThreeChecks[1] = 0;
        nRightTurnCalls = 0;


    }

    public void turnLeft(int nTicks){
        /*
        Loop up to the ticks incrementing the lock location
         */
        for(nTickCount = 1; nTickCount <= nTicks; nTickCount++){
            nTurnOnLock++;
            //if lock location equals 40 then reset it to 0
            if(nTurnOnLock == 40){
                nTurnOnLock = 0;
            }
        }
        nSecretTwoCheck = nTurnOnLock;


    }

    public void turnRight(int nTicks){
        /*
        Loop up to the ticks decrementing the lock location
        at each iteration
         */
        for(nTickCount = 1; nTickCount <= nTicks; nTickCount++){
            nTurnOnLock--;
            //if lock location is equal to -1 then reset it to 39
            if(nTurnOnLock == -1){
                nTurnOnLock = 39;
            }

        }

        nSecretOneAndThreeChecks[nRightTurnCalls] = nTurnOnLock;
        nRightTurnCalls++;

    }

    /*
    If the secrets are all equal reset and return true
    if not reset and return false so the user can try again
     */
    public boolean open(){
        if(( nSecretOneAndThreeChecks[0] != nSecretOne) || ( nSecretTwoCheck != nSecretTwo) || (nSecretOneAndThreeChecks[1] != nSecretThree)){
            reset();
        return false;
        }
        reset();
        return true;

    }
}
